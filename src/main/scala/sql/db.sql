CREATE TABLE IF NOT EXISTS "user"
(
    "id"    BIGSERIAL PRIMARY KEY,
    "age"   VARCHAR(255),
    "first" VARCHAR(255),
    "last"  VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS "salary"
(
    "id"    BIGSERIAL PRIMARY KEY,
    "user_id" BIGINT PRIMARY KEY REFERENCES "user" (id) ON DELETE CASCADE,
    "amount" INT NOT NULL
);