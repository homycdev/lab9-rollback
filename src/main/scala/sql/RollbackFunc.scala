package sql

import slick.dbio.Effect
import slick.jdbc.PostgresProfile.api._
import slick.model.ForeignKeyAction

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

object Tables {

  case class User(id: Int, age: Int, first: String, last: String)

  class UserTable(tag: Tag) extends Table[User](tag, "user") {
    def id: Rep[Int] = column("id", O.PrimaryKey)

    def age: Rep[Int] = column("age")

    def first: Rep[String] = column("first")

    def last: Rep[String] = column("last")

    override def * = (id, age, first, last).mapTo[User]
  }

  val userTableQuery = TableQuery[UserTable]

  case class Salary(id: Int, userId: Int, amount: Int)

  class SalaryTable(tag: Tag) extends Table[Salary](tag, "salary") {
    def id: Rep[Int] = column("id", O.PrimaryKey)

    def userId: Rep[Int] = column[Int]("user_id")

    def amount = column[Int]("amount", O.Default(0))

    lazy val userIdFK = foreignKey("user_id_fk", userId, userTableQuery)(
      r => r.id,
      onUpdate = ForeignKeyAction.NoAction,
      onDelete = ForeignKeyAction.Cascade
    )

    override def * = (id, userId, amount).mapTo[Salary]
  }

}
object EmployeeRepository {
  import Tables._
  val AllUsers = TableQuery[UserTable]
  val AllSalaries = TableQuery[SalaryTable]

  def addUser(user: User): DBIOAction[Int, NoStream, Effect.Write] =
    AllUsers += user

  def addSalary(salary: Salary) = {

    // Here we have some action to roll back in case of non existing user in Salaries Table

    val rollbackAction = (AllSalaries += salary).flatMap { _ =>
      DBIO.failed(new Exception("Roll it back"))
    }.transactionally

    // Handling error and not changing any data in Table

    val errorHandleAction = rollbackAction.asTry.flatMap {
      case Failure(e: Throwable) => DBIO.successful(e.getMessage)
      case Success(_)            => DBIO.successful("Fine")
    }
    errorHandleAction
  }

  def userSalary =
    AllSalaries
      .join(AllUsers)
      .on(_.userId === _.id)
      .result

}

object RollbackFunc extends App {
  import Tables._
  val db = Database.forConfig("mydb")
  val programme = for {
    _ <-
      (EmployeeRepository.AllUsers.schema ++ EmployeeRepository.AllSalaries.schema).create
    _ <- EmployeeRepository.AllUsers += User(1, 12, "FirstTest", "LastTEst")
    _ <- EmployeeRepository.AllSalaries += Salary(1, 1, 100)
    _ <- EmployeeRepository.AllSalaries += Salary(2, 1, 100) // Situation where we have an identical user. Won't change table, due to rollback action
    userSalary <- EmployeeRepository.userSalary
  } yield userSalary

  Await.result(db.run(programme).map(println), Duration.Inf)

}
